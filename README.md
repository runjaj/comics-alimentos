# Comics alimentos

## Introducción

Con mi limitado talento para el dibujo y la escritura he empezado a dibujar comics sobre algunos aspectos relativos con la ciencia y tecnología de los alimentos.

Por ahora, solo hay uno dedicado a las emulsiones. Intenta explicar brevemente que son, como se pueden hacer y que factores afectan a su estabilidad.

## Uso

Los comics se encuentran en varios formatos y se pueden utilizar para usos no comerciales citando a su autor. Se permiten cambios, no hace falta pedir permiso, solo hay que citar al autor y compartir con la misma licencia.

## Quien soy

Soy Javier Arántegui y doy clases en el departmento de tecnología de alimentos de la Univ. de Lleida.